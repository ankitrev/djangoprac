from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt
from views import StudentData


urlpatterns = [
	url(r'^read/(?P<id>\d+)$', csrf_exempt(StudentData.as_view()),
		name="put"),]
