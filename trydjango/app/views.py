from django.shortcuts import render
from django.http import HttpResponse, QueryDict
from django.views.generic import View
from .models import Student
from django.core.exceptions import ObjectDoesNotExist

class StudentData(View):

    def get(self,request):
        print("INSIDE GET FUNC")
        result=[]
        students=Student.objects.all()
        for student in students:
            res={}
            res['roll_no']=student.roll_no
            res['first_name']=student.first_name
            res['mobile_no']=student.mobile_no
            res['address']=student.address
            result.append(res)
        return HttpResponse(result, status=200)

    def post(self,request):
        print("PUT FUNC")
        obj=request.POST
        fname=obj['fn']
        lname=obj['ln']
        mbn=obj['mn'];
        ad=obj['add']
        roll=obj['roll']

        student_entry=Student(roll_no = roll, first_name = fname, last_name = lname, mobile_no = mbn, address = ad)
        student_entry.save()
        return HttpResponse('Done!')

    def delete(self,request):
        print('hello')
        obj=QueryDict(request.body)
        roll=obj['roll']
        e = Student.objects.filter(roll_no=roll)
        e.delete()
        #all_tasks.delete()
        return HttpResponse('done', status=200)

    def put(self,request,id):
        try:
            print('hii')
            update_req=QueryDict(request.body)
            data=Student.objects.get(id=id)

            if "first_name" in update_req.keys():
                data.first_name=update_req["first_name"]
            if "last_name" in update_req.keys():
                data.last_name=update_req["last_name"]
            if "mobile_no" in update_req.keys():
                data.mobile_no=update_req["mobile_no"]
            if "address" in update_req.keys():
                data.address=update_req["address"]
            data.save()
            return HttpResponse("Successfully updated", status=200)
        except ObjectDoesNotExist:
            return HttpResponse("Object does not exist", status=500)
        #except MultipleObjectsReturned:
            #return HttpResponse("multiple values returned", status=500)




# Create your views here.
