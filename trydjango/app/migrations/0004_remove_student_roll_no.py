# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0003_student_roll_no'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='student',
            name='roll_no',
        ),
    ]
